from django import forms
# Recopilación del listado de los municpios a utilizar
municipalities = [('Funza', 'Funza'), ('Soacha', 'Soacha'),
                  ('Bojacá', 'Bojacá'),('Santafe de Bogotá', 'Santafe de Bogotá')]
# Recopilación del listado de años a utilizar
years = [
    ('2019', '2019'),
    ('2018', '2018'),
    ('2017', '2017'),
    ('2016', '2016'),
    ('2015', '2015'),
    ('2014', '2014'),
    ('2013', '2013')]

# En esta sección se invoca una clase, los datos de ingreso son el municipio y el año
class MunicipalityForm(forms.Form):
    municipality = forms.CharField(label="Municipio", required=True,
                                   widget=forms.Select(
                                       attrs={'class': 'selectpicker', 'data-live-search':'true',
                                              'title': 'Municipio no seleccionado'},
                                       choices=municipalities)#De esta manera se configura la forma del contenido que se va a presentar al usuario, configuración de textos, tipos de letra y posición.
                                   )
    year = forms.CharField(label="Año", required=True,
                            widget=forms.Select(
                                attrs={'class': 'selectpicker', 'data-live-search': 'true',
                                       'title': 'Año no seleccionado'},
                                choices=years))

