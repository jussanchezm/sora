from django.shortcuts import render
from sora.forms import MunicipalityForm
from sora.model import data

months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre',
          'Noviembre', 'Diciembre']
precipitations = ['2', '2.02', '2.02', '2.03', '2.04', '2.05', '2.06', '2.07', '2.08', '2.09', '2.2', '2.22']

# Función encargada de mostar los datos al usuario, cuando el usuario no a presionado el boton, la grafica estará vacia, cuando el usuario ya ha presionado el boton, trae el formulario, evalua que sea valido y grafica los datos de las variables municipio vs año.
def graphs_home(request):
    global precipitations
    if request.method == 'POST':
        form = MunicipalityForm(request.POST)
        if form.is_valid():
            precipitations = data(request.POST.get("municipality"), request.POST.get("year"))
    else:
        form = MunicipalityForm()

    context = {
        'precipitations': precipitations,
        'months': months,
        'form': form
    }

    return render(request, 'graphs_home.html', context)
